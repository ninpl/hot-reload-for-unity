namespace SingularityGroup.HotReload {
    internal static class PackageConst {
        public const string Version = "1.1.9";
        public const string PackageName = "com.singularitygroup.hotreload";
    }
}
