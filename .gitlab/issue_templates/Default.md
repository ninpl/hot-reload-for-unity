#### Steps to reproduce:
<!-- Describe the steps you took that led to this issue: -->
<!-- If editing code failed, provide that code before and after -->


- start Hot Reload
- ...

#### What happened?

...

#### What should have happened?

...

#### Hot Reload Version and Operating System:

- Hot Reload Version: ... <!-- (see Help(or Settings) tab in Hot Reload window) -->
- Unity version: ...
- Operating System: ...
- Operating System version: ...

<!-- Example file:
Attach a sample file (or files) highlighting the issue, if appropriate. -->
