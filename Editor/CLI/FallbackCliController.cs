
using System.Threading.Tasks;

namespace SingularityGroup.HotReload.Editor.Cli {
    class FallbackCliController : ICliController {
        public Task Start(StartArgs args) => Task.CompletedTask;

        public Task Stop() => Task.CompletedTask;
    }
}