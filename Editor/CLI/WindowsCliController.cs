using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Debug = UnityEngine.Debug;

namespace SingularityGroup.HotReload.Editor.Cli {
    class WindowsCliController : ICliController {
        
        Process process;
        public Task Start(StartArgs args) {
            var robocopy = Process.Start(new ProcessStartInfo {
                FileName = "robocopy",
                Arguments = $@"""{args.executableSourceDir}"" ""{args.executableTargetDir}"" /purge /MIR",
                CreateNoWindow = true,
                UseShellExecute = false,
            });
            robocopy.WaitForExit();
            process = Process.Start(new ProcessStartInfo {
                FileName = Path.GetFullPath(Path.Combine(args.executableTargetDir, "CodePatcherCLI.exe")),
                Arguments = args.cliArguments,
            });
            return Task.CompletedTask;
        }

        public async Task Stop() {
            await RequestHelper.KillServer();
            try {
                process?.CloseMainWindow();
            } catch {
                //ignored
            }  
            process = null;
        }
    }
}