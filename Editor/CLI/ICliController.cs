using System.Threading.Tasks;

namespace SingularityGroup.HotReload.Editor.Cli {
    interface ICliController {
        Task Start(StartArgs args);
        Task Stop();
    }
}