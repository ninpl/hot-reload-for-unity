using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Debug = UnityEngine.Debug;

namespace SingularityGroup.HotReload.Editor.Cli {
    class OsxCliController : ICliController {
        Process process;
        
        public Task Start(StartArgs args) {
            var pidFilePath = CliUtils.GetPidFilePath(args.hotreloadTempDir);
            // To run in a Terminal window (so you can see compiler logs), we must put the arguments into a script file
            // and run the script in Terminal. Terminal.app does not forward the arguments passed to it via `open --args`.
            // *.command files are opened with the user's default terminal app.
            var executableScriptPath = Path.Combine(Path.GetTempPath(), "Start_HotReloadServer.command");
            // You don't need to copy the cli executable on mac
            // omit hashbang line, let shell use the default interpreter (easier than detecting your default shell beforehand)

            File.WriteAllText(executableScriptPath, $"echo $$ > \"{pidFilePath}\"" +
                                                    $"\ncd \"{Environment.CurrentDirectory}\"" + // set cwd because 'open' launches script with $HOME as cwd.
                                                    $"\n\"{args.executableSourcePath}\" {args.cliArguments}");
            CliUtils.Chmod(executableScriptPath); // make it executable
            CliUtils.Chmod(args.executableSourcePath); // make it executable

            //TODO is this really necessary?
            //This will require us to extract the dotnet sdk every startup which takes 3+ seconds
            //on top of the time it takes to delte all the files
            if (Directory.Exists(args.hotreloadTempDir)) {
                Directory.Delete(args.hotreloadTempDir, true);
            }
            
            Directory.CreateDirectory(args.hotreloadTempDir);
            Directory.CreateDirectory(args.executableTargetDir);
            Directory.CreateDirectory(args.cliTempDir);
            
            process = Process.Start(new ProcessStartInfo {
                FileName = "open",
                Arguments = $"'{executableScriptPath}'",
                UseShellExecute = true,
            });

            if (process.WaitForExit(1000)) {
                if (process.ExitCode != 0) {
                    Debug.LogWarning($"[{CodePatcher.TAG}] Failed to the run the start server command. ExitCode={process.ExitCode}\nFilepath: {executableScriptPath}");
                }
            }
            else {
                process.EnableRaisingEvents = true;
                process.Exited += (_, __) => {
                    if (process.ExitCode != 0) {
                        Debug.LogWarning($"[{CodePatcher.TAG}] Failed to the run the start server command. ExitCode={process.ExitCode}\nFilepath: {executableScriptPath}");
                    }
                };
            }
            return Task.CompletedTask;
        }

        public async Task Stop() {
            // kill HotReload server process (on mac it has different pid to the window which started it)
            await RequestHelper.KillServer();

            // process.CloseMainWindow throws if proc already exited.
            // We rely on the pid file for killing the trampoline script (in-case script is just starting and HotReload server not running yet)
            process = null;
            CliUtils.KillLastKnownHotReloadProcess();
        }
    }
}